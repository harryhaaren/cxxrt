
#ifndef CXXRT_LIST
#define CXXRT_LIST


class RtNode
{
  public:
    RtNode()
    {
      next = 0;
    }
    
    RtNode* next;
};



static RtNode* RtNodeRemoveAt(RtNode* head, int index)
{
  RtNode* n = head;
  RtNode* prev = 0;
  int nodeIndex = 0;
  
  while( n )
  {
    if ( index == nodeIndex )
    {
      // set the previous next to the current nodes next. That leaves out
      // one node in the middle: n
      prev->next = n->next;
      
      // return n to be deleted
      return n;
    }
    
    nodeIndex++;
    prev = n;
    n = n->next;
  }
  
  return 0;
}

// take head by ref, in case its an empty list, and we need to set head
static int RtNodeAppend(RtNode*& n, RtNode* insert)
{
  while( n )
  {
    if ( n->next == 0 )
    {
      n->next = insert;
      insert->next = 0;
      return 0;
    }
    n = n->next;
  }
  
  // list is empty, move head to here and done
  n = insert;
  
  return 0;
}

static int RtNodeInsertAfterIndex(RtNode*& head, int index, RtNode* insert)
{
  RtNode* n = head;
  RtNode* prev = 0;
  int nodeIndex = 0;
  
  while( n )
  {
    if ( index == nodeIndex )
    {
      // set insert's next to current next
      insert->next = n->next;
      
      // set current->next to insert
      n->next = insert;
      return 0;
    }
    
    nodeIndex++;
    prev = n;
    n = n->next;
  }
  
  //cout << "RtNodeInsertAt() warning: index is past end of list, appending" << endl;
  
  // prev can be NULL is head is NULL, or has only one element
  head = insert;
  
  return 0;
}

static int RtNodeSwapIndexs(RtNode* head, int index1, int index2)
{
  if ( index1 == index2 )
    return 0;
  
  else if ( index2 < index1 )
  {
    int tmp = index1;
    index1 = index2;
    index2 = tmp;
  }
  
  RtNode* n = head;
  RtNode* prev = 0;
  
  RtNode* node1prev = 0;
  RtNode* node2prev = 0;
  
  int i = 0;
  
  while( n )
  {
    if ( i == index1 )
    {
      node1prev = prev;
    }
    
    if ( i == index2 )
    {
      node2prev = prev;
    }
    
    i++;
    prev = n;
    n = n->next;
  }
  
  
  if ( node1prev && node2prev && node1prev->next && node2prev->next )
  {
    //cout << "RtNodeSwapIndexs() found both " << index1 << " " << index2 << endl;
    
    RtNode* node1 = node1prev->next;
    RtNode* node2 = node2prev->next;
    
    RtNode* node2next = node2->next;
    
    // put node2 into node1's position
    node1prev->next = node2;
    
    // if they're beside eachother, don't create loop
    if ( index2 - index1 > 1 )
    {
      node2->next = node1->next;
    } else {
      //cout << "RtNodeSwapIndexs() Nodes beside eachother! node2->next = node1 " << endl;
      node2->next = node1;
    }
    
    // put node1 into node2's position
    node2prev->next = node1;
    node1->next = node2next;
    
    
    return 0;
  }
  
  
  
  //cout << "RtNodeSwapIndexs() Error: didn't find indexes. No action performed! " << index1 << " " << index2 << endl;
  
  return -1;
}


static void RtNodeForEach(RtNode* n, void (*nodeCallback)(RtNode*, void*), void* userData = 0)
{
  // while node is not NULL
  while ( n )
  {
    // keep a copy of next: callback could deallocate current!
    RtNode* next = n->next;
    
    // call the provided callback on the node
    (*nodeCallback)(n, userData);
    
    // move to next RtNode
    n = next;
  }
}


#endif // CXXRT_LIST

