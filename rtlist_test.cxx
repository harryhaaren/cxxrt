#include "rtlist.hxx"

#include <iostream>
#include <cpptest.h>

using namespace std;

class MyNode : public RtNode
{
  public:
    MyNode(int d)
    {
      data = d;
    }
    
    int data;
};

void printCallback(RtNode* n, void*)
{
  MyNode* m = (MyNode*) n;
  
  cout << m->data << endl;
}

void deleteCallback(RtNode* n, void*)
{
  MyNode* m = (MyNode*) n;
  //cout << "delCB node " << m->data << endl;
  delete m;
}

class SomeTestSuite: public Test::Suite
{
public:
    SomeTestSuite() {
      TEST_ADD(SomeTestSuite::appendEmptyHead )
      TEST_ADD(SomeTestSuite::insertAfterEmptyHead )
      TEST_ADD(SomeTestSuite::insertBeyondEmptyHead )
      TEST_ADD(SomeTestSuite::swapEmptyHead )
      
      TEST_ADD(SomeTestSuite::append1Head )
      TEST_ADD(SomeTestSuite::insertAfter1Head )
      TEST_ADD(SomeTestSuite::insertBeyond1Head )
    }
private:
    
    RtNode* head;
    
    void setup()
    {
      head = 0;
    }
    void tear_down()
    {
      RtNodeForEach(head, &deleteCallback, 0 );
    }
    
    void appendToHead(int num)
    {
      for(int i = 0; i < num; i++)
        RtNodeAppend( head, new MyNode(0) );
    }
    
    // Empty head
    void appendEmptyHead()
    {
      RtNodeAppend( head, new MyNode(0) );
      TEST_ASSERT( head != 0);
    }
    void insertAfterEmptyHead()
    {
      RtNodeInsertAfterIndex( head, 0, new MyNode(0) );
      TEST_ASSERT( head != 0);
    }
    void insertBeyondEmptyHead()
    {
      RtNodeInsertAfterIndex( head, 21, new MyNode(0) );
      TEST_ASSERT( head != 0);
    }
    void swapEmptyHead()
    {
      int ret1 = RtNodeSwapIndexs( head, 2, 3 );
      TEST_ASSERT( ret1 == -1 );
      int ret2 = RtNodeSwapIndexs( head, 0, 0 );
      TEST_ASSERT( ret2 ==  0 ); // no effect, so should return 0
      int ret3 = RtNodeSwapIndexs( head, 0, 1 );
      TEST_ASSERT( ret3 == -1 );
      
      // useless test: if index's are the same, then it will return 0
      //int ret4 = RtNodeSwapIndexs( head, 1, 1 );
      //TEST_ASSERT( ret4 == -1 );
    }
    
    // 1 in head
    void append1Head()
    {
      appendToHead(1);
      RtNodeAppend( head, new MyNode(0) );
      TEST_ASSERT( head != 0);
    }
    void insertAfter1Head()
    {
      appendToHead(1);
      RtNodeInsertAfterIndex( head, 0, new MyNode(0) );
      TEST_ASSERT( head != 0);
    }
    void insertBeyond1Head()
    {
      appendToHead(1);
      RtNodeInsertAfterIndex( head, 21, new MyNode(0) );
      TEST_ASSERT( head != 0);
    }
    
    //
    void swap10Head()
    {
      appendToHead(10);
      int ret1 = RtNodeSwapIndexs( head, 2, 3 );
      TEST_ASSERT( ret1 == -1 );
    }
};

bool run_tests()
{
    SomeTestSuite sts;
    Test::TextOutput output(Test::TextOutput::Verbose);
    return sts.run(output);
}


int main()
{
  run_tests();
  
  return 0;
}
