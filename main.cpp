
// compile g++ main.cpp

// example code using a RtNode based pointer list:
// the object to be contained in the list is derived from RtNode
// list operations can be done using the provided static functions (see rtlist.hxx)

#include <iostream>

using namespace std;

#include "rtlist.hxx"

class MyNode : public RtNode
{
  public:
    MyNode(int d)
    {
      data = d;
    }
    
    int data;
};

void printCallback(RtNode* n, void*)
{
  MyNode* m = (MyNode*) n;
  
  cout << m->data << endl;
}

int main()
{
  RtNode* head = 0;
  
  RtNodeForEach(head, &printCallback );
  
  //head = new MyNode(0);
  
  int r = RtNodeAppend( head, new MyNode(0) );
  cout << "r = " << r << "  head now = " << head << endl;
  r = RtNodeAppend( head, new MyNode(1) );
  cout << "r = " << r << endl;
  
  cout << "After append" << endl;
  RtNodeForEach(head, &printCallback );
  
  /*
  int i = 0;
  //for ( int i = 0; i < 10; i++ )
    RtNodeInsertAfterIndex( head, i, new MyNode(i+1) );
  
  cout << "After loop insertAfterIndex" << endl;
  RtNodeForEach(head, &printCallback);
  
  RtNodeSwapIndexs( head, 2, 3 );
  
  RtNodeForEach(head, &printCallback );
  
  RtNodeSwapIndexs( head, 2, 5 );
  
  RtNodeForEach(head, &printCallback );
  
  RtNodeSwapIndexs( head, 5, 2 );
  
  RtNodeForEach(head, &printCallback );
  
  RtNodeSwapIndexs( head, 3, 2 );
  */
  
  cout << "Final print" << endl;
  RtNodeForEach(head, &printCallback );
  
  
  
  return 0;
  
}
